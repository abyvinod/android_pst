Android Point and Shoot Translator
==================================

We are a team of 4 undergraduate students studying in IIT Madras, who qualified for the final round of the prestigious TATA Enginx Innovation competition. We submitted an Android application for translating the American Sign Language used by people who have speaking disability.

The user needs to wear a glove which is color-coded for image processing. The gestures were identified using OpenCV library. Once the words were constructed, the application uses Google Text-to-Speech software for reading out the message.