package iitm.bang.android_pst;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ScratchPad extends Activity implements TextToSpeech.OnInitListener{
	TextToSpeech tts;
	String text;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scratch_pad);
		final Button speak = (Button) findViewById(R.id.buttonSpeak);
		final Button back = (Button) findViewById(R.id.buttonBack);
		final EditText speakOutText = (EditText) findViewById(R.id.ScratchSpeakout);
		tts = new TextToSpeech(this, this);
		
		speak.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String dummy =speakOutText.getText().toString();
				Log.d("ScratchSpeak", dummy);
				speakOut(dummy);
			}
		});
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.scratch_pad, menu);
		return true;
	}

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		Log.d("Scratch", "TTS Initialising");
		if (status == TextToSpeech.SUCCESS) {

			int result = tts.setLanguage(Locale.US);

			// tts.setPitch(5); // set pitch level

			// tts.setSpeechRate(2); // set speech speed rate

			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e("TTS", "Language is not supported");
			} else {
				//btnSpeak.setEnabled(true);
				//speakOut("This is great.");
			}

		} else {
			Log.e("TTS", "Initilization Failed");
		}
		Log.d("Scratch", "TTS Initialised");
	}
	private void speakOut(String t) {

		//String text = txtText.getText().toString();

		tts.speak(t, TextToSpeech.QUEUE_FLUSH, null);
		
	}
}
