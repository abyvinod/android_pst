package iitm.bang.android_pst;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class Settings extends Activity {
	
	SettingsDBAdapter set;
	Integer History;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		final Button save = (Button) findViewById(R.id.button1);
		final CheckBox check = (CheckBox) findViewById(R.id.checkBox1);
		final EditText callout = (EditText) findViewById(R.id.editText1);
		set = new SettingsDBAdapter(getApplicationContext());
		set.updatemem();
		check.setChecked(true);
		if(set.callout.equals(""))
		{
			callout.setHint("Please enter the callout text");
		}
		else
		{
			callout.setText(set.callout);
		}
		if(set.history==true)
		{
			check.setChecked(true);
		}            
		else
		{
			check.setChecked(false);
		}
		
		save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(check.isChecked())
				{
					History = 1;
				}
				else 
				{
					History = 0;
				}
				set.updateset(callout.getText().toString(), History);
				Toast.makeText(getApplicationContext(), "Settings updated", Toast.LENGTH_SHORT).show();
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);
		return true;
	}

}
