package iitm.bang.android_pst;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.os.Bundle;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.graphics.Color;
import android.hardware.Camera;

public class Calibration extends Activity {
	
	protected static final int CAMERA_REQUEST = 1888;
	private ImageView imageView;
	private float hmin=0, hmax=0, smin=0, smax=0, vmin=0, vmax=0;
//	private float havg=0, savg=0, vavg=0, hsd=0, ssd=0, vsd=0;
	private float[] hsv= new float[3];
	public Button cont;
	public Bitmap photo=null;
	public ColourDBAdapter cdb;
	String colour;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calibration);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
		Bundle b=getIntent().getExtras();
		colour=b.getString("Colour");
		Log.d("bolor", colour);
		this.imageView = (ImageView)this.findViewById(R.id.imageView1);
        Button photoButton = (Button) this.findViewById(R.id.button1);
        cont = (Button) this.findViewById(R.id.button2);
        cont.setVisibility(4);
        cdb=new ColourDBAdapter(getApplicationContext());
        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
/*            	 Camera.CameraInfo info = new Camera.CameraInfo();
        	     Camera.getCameraInfo(0, info);
        	     int rotation = getWindowManager().getDefaultDisplay().getRotation();
        	     int degrees = 0;
        	     switch (rotation) {
        	         case Surface.ROTATION_0: degrees = 0; break;
        	         case Surface.ROTATION_90: degrees = 90; break;
        	         case Surface.ROTATION_180: degrees = 180; break;
        	         case Surface.ROTATION_270: degrees = 270; break;
        	     }

        	     int result;
        	     Camera camera=Camera.open();
        	     if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
        	         result = (info.orientation + degrees) % 360;
        	         result = (360 - result) % 360;  // compensate the mirror
        	     } else {  // back-facing
        	         result = (info.orientation - degrees + 360) % 360;
        	     }
        	     camera.setDisplayOrientation(result);
            	 camera.release();*/
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE); 
                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
            }
        });
	}
        
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
            if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {  
                photo = (Bitmap) data.getExtras().get("data"); 
                photo = photo.copy(Bitmap.Config.ARGB_8888, true); 
                final Bitmap pcropd=Bitmap.createBitmap(photo, (int) 3*photo.getWidth()/8,(int) 3*photo.getHeight()/8,(int) photo.getWidth()/4,(int) photo.getHeight()/4);
                Mat imageMat = new Mat();
                Utils.bitmapToMat(pcropd, imageMat);
                Imgproc.GaussianBlur(imageMat, imageMat, new Size(5,5), 0.1);
                Utils.matToBitmap(imageMat, pcropd);
                imageView.setImageBitmap(pcropd);
                //Mat newmat = null ;
                //Utils.bitmapToMat(photo,newmat);
                //Mat mHSV = new Mat();
                //Imgproc.cvtColor(newmat, mHSV, Imgproc.COLOR_BGR2HSV,3);
                //Utils.matToBitmap(mHSV, photo);
                //photo.get
                Log.d("size", "Size="+photo.getHeight()+"   " + photo.getWidth());
                cont.setVisibility(0);
                cont.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						for(int i=0; i<pcropd.getHeight();i++)
		                {
		                	for(int j=0; j<pcropd.getWidth();j++)
		                	{
		                		Color.colorToHSV(pcropd.getPixel(j, i), hsv);
		                		//if(hsv[0]>320){
		                		//	hsv[0]=0;
		                		//}
		                		if(i==0 && j==0)
		                		{
		                			hmin=hsv[0];
		                			hmax=hsv[0];
		                			smin=hsv[1];
		                			smax=hsv[1];
		                			vmin=hsv[2];
		                			vmax=hsv[2];
		                		}
		                		if(hsv[0]<hmin)
		                		{
		                			hmin=hsv[0];
		                		}
		                		if(hsv[0]>hmax)
		                		{
		                			hmax=hsv[0];
		                		}
		                		if(hsv[1]<smin)
		                		{
		                			smin=hsv[1];
		                		}
		                		if(hsv[1]>smax)
		                		{
		                			smax=hsv[1];
		                		}
		                		if(hsv[2]<vmin)
		                		{
		                			vmin=hsv[2];
		                		}
		                		if(hsv[2]>vmax)
		                		{
		                			vmax=hsv[2];
		                		}
		                	}
		                }
		                Log.d("Color", "h=" + (hmax+hmin)/2);
		                Log.d("Color", "s=" + (smax+smin)/2);
		                Log.d("Color", "v=" + (vmax+vmin)/2);
		                Log.d("Color", "hr=" + (hmax-hmin)/2);
		                Log.d("Color", "sr=" + (smax-smin)/2);
		                Log.d("Color", "vr=" + (vmax-vmin)/2);
		                Log.d("Color", "m=" + (hmax) + " " + hmin);
		                //Intent i=getIntent();
		                //i.putExtra("h", (hmax+hmin)/2);
		                //i.putExtra("s", (smax+smin)/2);
		                //i.putExtra("v", (vmax+vmin)/2);
		                //i.putExtra("hrad", (hmax-hmin)/2);
		                //i.putExtra("srad", (smax-smin)/2);
		                //i.putExtra("vrad", (vmax-vmin)/2);
		                //setResult(RESULT_OK, i);
		                int Hue=(int) (((hmax+hmin)/2)*255/360);
		                int Sat=(int) (((smax+smin)/2)*255);
		                int Val=(int) (((vmax+vmin)/2)*255);
		                int HueR=(int) (((hmax-hmin)/2)*255/360+15);
		                int SatR=(int) (((smax-smin)/2)*255+30);
		                int ValR=(int) (((vmax-vmin)/2)*255+50);
		                //int ValR=(int) 255;
		                cdb.updateC(colour, Hue, Sat, Val, HueR, SatR, ValR);
		                Intent i = new Intent(getApplicationContext(), ColourDBActivity.class);
		                startActivity(i);
		                finish();
					}
				});
                
            }  
    } 
}
