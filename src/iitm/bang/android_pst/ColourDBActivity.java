package iitm.bang.android_pst;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ColourDBActivity extends Activity {

	ColourDBAdapter cdb;
	Context context=this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_colour_db);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        cdb=new ColourDBAdapter(context);
		
		TextView t1=(TextView) findViewById(R.id.TextView1);
		Button b1=(Button) findViewById(R.id.button1);
		Cursor c=cdb.getAllCs();
		
		String str="The following are the info stored in DB:\n Colour | Hue | Sat | Val | HueR | SatR | ValR | Counter\n";
		
		while(c.moveToNext())
		{
			str=str+c.getString(0)+" | "+c.getInt(1)+" | "+c.getInt(2)+" | "+c.getInt(3)+" | "+c.getInt(4)+" | "+c.getInt(5)+" | "+c.getInt(6)+" | "+c.getInt(7)+"\n";
		}
		c.close();
		t1.setText(str);
		
		b1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				// TODO Auto-generated method stub
				Intent i=new Intent(getApplicationContext(),MainActivity.class);
				startActivity(i);
//				Toast.makeText(context, "Dei!!! Restart the app if you did some calibration.", Toast.LENGTH_SHORT).show();
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.colour_db, menu);
		return true;
	}

}
