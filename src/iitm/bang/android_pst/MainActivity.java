package iitm.bang.android_pst;

import java.util.Locale;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import android.R.string;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;
import android.view.textservice.TextServicesManager;
import android.view.textservice.SpellCheckerSession.SpellCheckerSessionListener;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SpellCheckerSession;
import android.widget.Toast;
import java.lang.StringBuilder;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class MainActivity extends Activity implements CvCameraViewListener2, TextToSpeech.OnInitListener, SpellCheckerSessionListener{
    private Mat                 mRgba;    
    private patchDetect    	  	pDetector;
    public int 					GET_HSV_REQUEST=1;
    public int start=0;
    private TextToSpeech 		tts;
    private TextServicesManager tsm;
    private String speakString="";
    private String speakString_prev="";
    //Variable for TTS
    private String text=new String();
    private SpellCheckerSession mScs;
    private boolean debug = true;
    private Menu menu;
    SettingsDBAdapter set;
    
    String Colour[] ={"Pink","LBlue","Yellow","Green","Orange","Black","Red","Blue","DGreen"};
    
    private CameraBridgeViewBase mOpenCvCameraView;

    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.d("Main", "OpenCV loaded successfully.");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public MainActivity() {
        Log.d("Main", "Instantiated new " + this.getClass());
        
        
    }

    /** Called when the activity is first created. */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_main);
		//TTS
		set=new SettingsDBAdapter(getApplicationContext());
		set.updatemem();
        
		tts = new TextToSpeech(getApplicationContext(), this);
        text = set.callout;
        Log.d("OnCreate", text);
        Log.d("History", ""+set.history);
        //speakOut("Hi");//Cup thing doesn't speak this out!!!
        Log.d("sptxt",text);
        tsm = (TextServicesManager) getSystemService(Context.TEXT_SERVICES_MANAGER_SERVICE);
        mScs = tsm.newSpellCheckerSession(null, null, this, true);
    	Log.d("Main", "called onCreate");
        
        
        
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.activity_main);
        mOpenCvCameraView.setCvCameraViewListener(this);

//        fpsmeter=new FpsMeter();
//        fpsmeter.init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu1) {
    	menu1.addSubMenu("Settings");
    	menu1.addSubMenu("Debugger Options");
    	menu1.addSubMenu("Scratchpad");
		menu1.addSubMenu("Calib Data");
    	menu1.addSubMenu(Colour[0]);
    	menu1.addSubMenu(Colour[1]);
    	menu1.addSubMenu(Colour[2]);
    	menu1.addSubMenu(Colour[3]);
    	menu1.addSubMenu(Colour[4]);
    	menu1.addSubMenu(Colour[5]);
    	menu1.addSubMenu(Colour[6]);
    	menu1.addSubMenu(Colour[7]);
	   	menu = menu1;
    	return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	if(item.getTitle().toString().equals("Settings"))
    	{
    		Intent i0 = new Intent(getApplicationContext(), Settings.class);
    		startActivity(i0);
    	}
    	else if(item.getTitle().toString().equals("Debugger Options"))
    	{
    		if(debug == false)
    		{
    			debug = true;
    		}
    		else
    		{
    			debug = false;
    		}
    		onPrepareOptionsMenu(menu);
    	}
    	else if(item.getTitle().toString().equals("Calib Data"))
    	{
            Intent i1 = new Intent(getApplicationContext(), ColourDBActivity.class);
            startActivity(i1);
    	}
    	else if(item.getTitle().toString().equals("Scratchpad"))
    	{
    		Intent i2 = new Intent(getApplicationContext(), ScratchPad.class);
            startActivity(i2);
    	}
    	else
    	{
	    	Toast.makeText(this,"Calibrating "+item.getTitle(), Toast.LENGTH_SHORT).show();
	    	Intent takepic = new Intent(getApplicationContext(), Calibration.class);
	        Bundle bund = new Bundle();
			bund.putString("Colour", item.getTitle().toString());
		    takepic.putExtras(bund);
			startActivity(takepic);
			finish();
	        //Intent takepic = new Intent(getApplicationContext(), Calibration.class);
	        //startActivityForResult(takepic, GET_HSV_REQUEST);
	    	//pDetector.calibrate(item.getItemId());
    	}
    	return true;
    }
    
   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	// TODO Auto-generated method stub
    	super.onActivityResult(requestCode, resultCode, data);
    	
    }*/

    
    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
//        final TextServicesManager tsm = (TextServicesManager) getSystemService(
//                Context.TEXT_SERVICES_MANAGER_SERVICE);
//        mScs = tsm.newSpellCheckerSession(null, null, this, true);
//        if (mScs != null) {
//            // Instantiate TextInfo for each query
//            // TextInfo can be passed a sequence number and a cookie number to identify the result
//            mScs.getSuggestions(new TextInfo("tgis"), 1);
//        	mScs.getSuggestions(new TextInfo("hllo"), 1);
//        	mScs.getSuggestions(new TextInfo("helloworld"), 1);
//        } else {
//            Log.e("SpellChecker", "Couldn't obtain the spell checker service.");
//        }
    }

    public void onDestroy() {
    	if (tts != null) {
			tts.stop();
			tts.shutdown();
		}
    	if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    	super.onDestroy();
    }

    public void onCameraViewStarted(int width, int height) {
    	
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        pDetector= new patchDetect(getApplicationContext());
        pDetector.showColourBoxes(mRgba);
    }

    public void onCameraViewStopped() {
        mRgba.release();
    }
    
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
    	Log.d("Main","New Frame received");
//        fpsmeter.measure();
        mRgba = inputFrame.rgba();
        pDetector.updatevalues(mRgba);
        //Log.d("OnCameraFrameSpeakOut", "Test");
        speakString=pDetector.Spout;
        if (start==0)
    	{
    		Log.d("OnCameraFrameSpeakOut", text);
    		speakOut(text);
//    		Log.d("OnCameraFrameSpeakOut", "Hi");
//    		speakOut("Hi");
    		start=1;
    	}
        if (mScs != null) {
        	Log.d("OnCameraFrameSpeakOut", "Nikhil");
            // Instantiate TextInfo for each query
            // TextInfo can be passed a sequence number and a cookie number to identify the result
        	
            if (!speakString.equalsIgnoreCase(speakString_prev))
            {
            	//Log.d("Suggestion1",speakString);
            	Log.d("OnCameraFrameSpeakOut", "Test");
            	mScs.getSuggestions(new TextInfo(speakString), 1);
            	//mScs.getSuggestions(new TextInfo("Test"), 1);
            	speakString_prev=speakString;
            }          
        } else {
            Log.e("SpellChecker", "Couldn't obtain the spell checker service.");
            Log.d("OnCameraFrameSpeakOut", "Spell Checker not working");
        }
        
        return mRgba;
    }

	@Override
	public void onInit(int status) {
		// TODO Auto-generated method stub
		if (status == TextToSpeech.SUCCESS) {

			int result = tts.setLanguage(Locale.US);

			// tts.setPitch(5); // set pitch level

			// tts.setSpeechRate(2); // set speech speed rate

			if (result == TextToSpeech.LANG_MISSING_DATA
					|| result == TextToSpeech.LANG_NOT_SUPPORTED) {
				Log.e("TTS", "Language is not supported");
			} else {
				//btnSpeak.setEnabled(true);
				//speakOut(text);
			}

		} else {
			Log.e("TTS", "Initilization Failed");
		}

	}

	private void speakOut(String t) {

		//String text = txtText.getText().toString();
		Log.d("methodSpeak", "summa "+t+" summa");
		tts.speak(t, TextToSpeech.QUEUE_ADD, null);
		Log.d("methodSpeak", "summa "+t+" summa");
	}
	
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
    public void onGetSuggestions(final SuggestionsInfo[] arg0) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arg0.length; ++i) {
            // Returned suggestions are contained in SuggestionsInfo
            final int len = arg0[i].getSuggestionsCount();
//            sb.append('\n');
            for (int j = 0; j < len; ++j) {
                sb.append(arg0[i].getSuggestionAt(j));
            }
//            sb.append(" (" + len + ")");
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                speakOut(sb.toString());
            	Log.d("Suggestion", sb.toString());
            }
        });
    }

	@Override
	public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] results) {
		// TODO Auto-generated method stub
		
	}
	public boolean onPrepareOptionsMenu (Menu menu) {    
	    menu.clear();    
	    if (debug)
	    {
	    	Log.d("Menu", "debugger on");
	        menu.clear();
			menu.addSubMenu("Settings");
        	menu.addSubMenu("Debugger Options");
        	menu.addSubMenu("Scratchpad");
	    	menu.addSubMenu("Calib Data");
	    	menu.addSubMenu(Colour[0]);
	    	menu.addSubMenu(Colour[1]);
	    	menu.addSubMenu(Colour[2]);
	    	menu.addSubMenu(Colour[3]);
	    	menu.addSubMenu(Colour[4]);
	    	menu.addSubMenu(Colour[5]);
	    	menu.addSubMenu(Colour[6]);
	    	menu.addSubMenu(Colour[7]);
    	}
	    else 
	    {
	    	Log.d("Menu", "debugger off");
	        menu.clear();
			menu.addSubMenu("Settings");
        	menu.addSubMenu("Debugger Options");
        	menu.addSubMenu("Scratchpad");
    	}    
	    return super.onPrepareOptionsMenu(menu);
	}
}
