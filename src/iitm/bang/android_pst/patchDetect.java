package iitm.bang.android_pst;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class patchDetect {
	
	private static double mMinContourArea = 0.4;
    private String TAG="Android_PST:patchDetect";
    public ColourDBAdapter cdb;
//    private int Cidx=0;
    private int Hidx=1;
    private int Sidx=2;
    private int Vidx=3;
    private int HRidx=4;
    private int SRidx=5;
    private int VRidx=6;
//    private int CRidx=7;
    
    public double centroidx;
    public double centroidy;
    public double sqside;
    public int colour1count;
    public int colour2count;
    public int colour3count;
    public int colour4count;
    public int colour5count;
    public int colour6count;
    public int colour7count;
    public int colour8count;
    
    public String Spout;
    
    public boolean clench;
    public int oc;
    public boolean isStraight;
    
    
    List<RotatedRect> colour1RR = new ArrayList<RotatedRect>();
    List<RotatedRect> colour2RR = new ArrayList<RotatedRect>();
    List<RotatedRect> colour3RR = new ArrayList<RotatedRect>();
    List<RotatedRect> colour4RR = new ArrayList<RotatedRect>();
    List<RotatedRect> colour5RR = new ArrayList<RotatedRect>();
    List<RotatedRect> colour6RR = new ArrayList<RotatedRect>();
    List<RotatedRect> colour7RR = new ArrayList<RotatedRect>();
    List<RotatedRect> colour8RR = new ArrayList<RotatedRect>();
    
    //To see colour mappings, refer to config detect line13
    //Try to follow this convention!
    //Colour6 is index, ring backside
    //Colour3 is middle, little backside
    //Colour1 is index, ring front
    //Colour2 is middle, little front
    //Colour8 is ROI colour
    ConfigDetect cDetector = new ConfigDetect(colour3RR, colour2RR, colour6RR, colour1RR, colour8RR, colour5RR, colour7RR, colour4RR);
    
    String Colour[] ={"Pink","LBlue","Yellow","Green","Orange","Black","Red","Blue","DGreen"};
    // Ensure the colours match through
    //Storing the Contour colours in RGB.
    private Scalar colour1ContourColour=new Scalar(0,0,0,255);//Pink is black
    private Scalar colour2ContourColour=new Scalar(180,180,0,255);//LBlue is yellow
	private Scalar colour3ContourColour= new Scalar(255,0,0,255);//Yellow is red
	private Scalar colour4ContourColour=new Scalar(0,0,255,255);//Green (ROI) is blue
	private Scalar colour5ContourColour= new Scalar(0,255,0,255); // Same currently used for ROI too :(
	private Scalar ROIContourColour= new Scalar(0,255,0,255);//Orange is green     
	//TODO: Ankit get good contour colours here please
	private Scalar colour6ContourColour= new Scalar(255,255,255,255);//Dark green is yellow
	private Scalar colour7ContourColour=new Scalar(0,127,127,255);
	private Scalar colour8ContourColour= new Scalar(127,0,127,255);

	// Don't touch these the code will update them appropriately
	public Scalar mcolour1LowerBound = new Scalar(0);
    public Scalar mcolour1UpperBound = new Scalar(0);
    public Scalar mcolour2LowerBound = new Scalar(0);
    public Scalar mcolour2UpperBound = new Scalar(0);
    public Scalar mcolour3LowerBound = new Scalar(0);
    public Scalar mcolour3UpperBound = new Scalar(0);
    public Scalar mcolour4LowerBound = new Scalar(0);
    public Scalar mcolour4UpperBound = new Scalar(0);
    public Scalar mcolour5LowerBound = new Scalar(0);
    public Scalar mcolour5UpperBound = new Scalar(0);
    public Scalar mcolour6LowerBound = new Scalar(0);
    public Scalar mcolour6UpperBound = new Scalar(0);
    public Scalar mcolour7LowerBound = new Scalar(0);
    public Scalar mcolour7UpperBound = new Scalar(0);
    public Scalar mcolour8LowerBound = new Scalar(0);
    public Scalar mcolour8UpperBound = new Scalar(0);

	//Declaring the color labels
	private Mat colour1colorLabel;
	private Mat colour2colorLabel;
	private Mat colour3colorLabel;
	private Mat colour4colorLabel;
	private Mat colour5colorLabel;
	private Mat colour6colorLabel;
	private Mat colour7colorLabel;
	private Mat colour8colorLabel;

	// Mats required for updatevalues()
	Mat mPyrDownMat = new Mat();	// The mid value of blurring
    Mat mHsvMat = new Mat();		// Post blurring. Common for all colours
    Mat mHSVMatMain=new Mat();
    
    Mat mcolour1Mask = new Mat();										// Binary image
    Mat mcolour1DilatedMask = new Mat();								// Post dilation
    Mat mcolour1Hierarchy = new Mat();								// Keeps track of the contour loops
    
    Mat mcolour2Mask = new Mat();										// Binary image
    Mat mcolour2DilatedMask = new Mat();								// Post dilation
    Mat mcolour2Hierarchy = new Mat();								// Keeps track of the contour loops

    Mat mcolour3Mask = new Mat();										// Binary image
    Mat mcolour3DilatedMask = new Mat();								// Post dilation
    Mat mcolour3Hierarchy = new Mat();								// Keeps track of the contour loops
    
    Mat mcolour4Mask = new Mat();										// Binary image
    Mat mcolour4DilatedMask = new Mat();								// Post dilation
    Mat mcolour4Hierarchy = new Mat();								// Keeps track of the contour loops

    Mat mcolour5Mask = new Mat();										// Binary image
    Mat mcolour5DilatedMask = new Mat();								// Post dilation
    Mat mcolour5Hierarchy = new Mat();								// Keeps track of the contour loops

    Mat mcolour6Mask = new Mat();										// Binary image
    Mat mcolour6DilatedMask = new Mat();								// Post dilation
    Mat mcolour6Hierarchy = new Mat();								// Keeps track of the contour loops
    
    Mat mcolour7Mask = new Mat();										// Binary image
    Mat mcolour7DilatedMask = new Mat();								// Post dilation
    Mat mcolour7Hierarchy = new Mat();								// Keeps track of the contour loops

    Mat mcolour8Mask = new Mat();										// Binary image
    Mat mcolour8DilatedMask = new Mat();								// Post dilation
    Mat mcolour8Hierarchy = new Mat();								// Keeps track of the contour loops

    List<MatOfPoint> mcolour1contours = new ArrayList<MatOfPoint>(); // Stores the contour loops  
    List<MatOfPoint> mcolour2contours = new ArrayList<MatOfPoint>();  
    List<MatOfPoint> mcolour3contours = new ArrayList<MatOfPoint>();
    List<MatOfPoint> mcolour4contours = new ArrayList<MatOfPoint>();
    List<MatOfPoint> mcolour5contours = new ArrayList<MatOfPoint>();
    List<MatOfPoint> mcolour6contours = new ArrayList<MatOfPoint>();
    List<MatOfPoint> mcolour7contours = new ArrayList<MatOfPoint>();
    List<MatOfPoint> mcolour8contours = new ArrayList<MatOfPoint>();
    
    /*Mat imgold=new Mat();
    Mat imgnew=new Mat();
    Mat imgres=new Mat();
    public Scalar res=new Scalar(255,255,255,255);
    public int cnt=0;*/ 
    
    MatOfPoint2f m1c=new MatOfPoint2f();
    MatOfPoint2f m2c=new MatOfPoint2f();
    MatOfPoint2f m3c=new MatOfPoint2f();
    MatOfPoint2f m4c=new MatOfPoint2f();
    MatOfPoint2f m5c=new MatOfPoint2f();
    MatOfPoint2f m6c=new MatOfPoint2f();
    MatOfPoint2f m7c=new MatOfPoint2f();
    MatOfPoint2f m8c=new MatOfPoint2f();

        
    // For the rotated ellipse
    MatOfPoint2f maxMOP2f = new MatOfPoint2f();
	public patchDetect(Context context)
	{
		cdb=new ColourDBAdapter(context);
		updateBounds();
        
	}
	
	public void updateBounds()
	{
		Log.d(TAG,"Updating HSV for "+Colour[0]);
		Cursor c=cdb.getC(Colour[0]);
		Scalar HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		Scalar Radius=new Scalar(c.getInt(HRidx),c.getInt(SRidx),c.getInt(VRidx),255);
		setHsvColor(HSV,mcolour1LowerBound,mcolour1UpperBound,Radius);
		c.close();
		
		Log.d(TAG,"Updating HSV for "+Colour[1]);
		c=cdb.getC(Colour[1]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		Radius=new Scalar(c.getInt(HRidx),c.getInt(SRidx),c.getInt(VRidx),255);
		setHsvColor(HSV,mcolour2LowerBound,mcolour2UpperBound,Radius);
		c.close();

		Log.d(TAG,"Updating HSV for "+Colour[2]);
		c=cdb.getC(Colour[2]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		Radius=new Scalar(c.getInt(HRidx),c.getInt(SRidx),c.getInt(VRidx),255);
		setHsvColor(HSV,mcolour3LowerBound,mcolour3UpperBound,Radius);
		c.close();

		Log.d(TAG,"Updating HSV for "+Colour[3]);
		c=cdb.getC(Colour[3]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		Radius=new Scalar(c.getInt(HRidx),c.getInt(SRidx),c.getInt(VRidx),255);
		setHsvColor(HSV,mcolour4LowerBound,mcolour4UpperBound,Radius);
		c.close();

		Log.d(TAG,"Updating HSV for "+Colour[4]);
		c=cdb.getC(Colour[4]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		Radius=new Scalar(c.getInt(HRidx),c.getInt(SRidx),c.getInt(VRidx),255);
		setHsvColor(HSV,mcolour5LowerBound,mcolour5UpperBound,Radius);
		c.close();

		Log.d(TAG,"Updating HSV for "+Colour[5]);
		c=cdb.getC(Colour[5]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		Radius=new Scalar(c.getInt(HRidx),c.getInt(SRidx),c.getInt(VRidx),255);
		setHsvColor(HSV,mcolour6LowerBound,mcolour6UpperBound,Radius);
		c.close();

		Log.d(TAG,"Updating HSV for "+Colour[6]);
		c=cdb.getC(Colour[6]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		Radius=new Scalar(c.getInt(HRidx),c.getInt(SRidx),c.getInt(VRidx),255);
		setHsvColor(HSV,mcolour7LowerBound,mcolour7UpperBound,Radius);
		c.close();

		Log.d(TAG,"Updating HSV for "+Colour[7]);
		c=cdb.getC(Colour[7]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		Radius=new Scalar(c.getInt(HRidx),c.getInt(SRidx),c.getInt(VRidx),255);
		setHsvColor(HSV,mcolour8LowerBound,mcolour8UpperBound,Radius);
		c.close();
	}
	
    public void setHsvColor(Scalar hsvColor, Scalar mLowerBound, Scalar mUpperBound, Scalar mColorRadius) {
        double minH = (hsvColor.val[0] >= mColorRadius.val[0]) ? hsvColor.val[0]-mColorRadius.val[0] : 0;
        double maxH = (hsvColor.val[0]+mColorRadius.val[0] <= 255) ? hsvColor.val[0]+mColorRadius.val[0] : 255;

        mLowerBound.val[0] = minH;
        mUpperBound.val[0] = maxH;

        mLowerBound.val[1] = hsvColor.val[1] - mColorRadius.val[1];
        mUpperBound.val[1] = hsvColor.val[1] + mColorRadius.val[1];

        mLowerBound.val[2] = hsvColor.val[2] - mColorRadius.val[2];
        mUpperBound.val[2] = hsvColor.val[2] + mColorRadius.val[2];

        mLowerBound.val[3] = 0;
        mUpperBound.val[3] = 255;
        Log.d(TAG,"HSV set");
    }

	public void showColourBoxes(Mat mRgba)
	{
		Log.d(TAG,"Getting colour "+Colour[1]);
		Cursor c=cdb.getC(Colour[0]);
		Scalar HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		colour1colorLabel = mRgba.submat(4, 28, 4, 32);
		colour1colorLabel.setTo(converScalarHsv2Rgba(HSV));
		colour1colorLabel = mRgba.submat(28, 40, 4, 32);
		colour1colorLabel.setTo(colour1ContourColour);
		c.close();
		
		Log.d(TAG,"Getting colour "+Colour[1]);
		c=cdb.getC(Colour[1]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		colour2colorLabel = mRgba.submat(4, 28, 32, 60);
		colour2colorLabel.setTo(converScalarHsv2Rgba(HSV));
		colour2colorLabel = mRgba.submat(28, 40, 32, 60);
		colour2colorLabel.setTo(colour2ContourColour);
		c.close();
		
		Log.d(TAG,"Getting colour "+Colour[2]);
		c=cdb.getC(Colour[2]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		colour3colorLabel = mRgba.submat(4, 28, 60, 88);
		colour3colorLabel.setTo(converScalarHsv2Rgba(HSV));
		colour3colorLabel = mRgba.submat(28, 40,60,88);
		colour3colorLabel.setTo(colour3ContourColour);
		c.close();
		
		Log.d(TAG,"Getting colour "+Colour[3]);
		c=cdb.getC(Colour[3]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		colour4colorLabel = mRgba.submat(4, 28, 88, 116);
		colour4colorLabel.setTo(converScalarHsv2Rgba(HSV));
		colour4colorLabel = mRgba.submat(28, 40,88, 116);
		colour4colorLabel.setTo(colour4ContourColour);
		c.close();
		
		Log.d(TAG,"Getting colour "+Colour[4]);
		c=cdb.getC(Colour[4]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		colour5colorLabel = mRgba.submat(4, 28, 116, 144);
		colour5colorLabel.setTo(converScalarHsv2Rgba(HSV));
		colour5colorLabel = mRgba.submat(28, 40,116, 144);
		colour5colorLabel.setTo(colour5ContourColour);
		c.close();
		
		Log.d(TAG,"Getting colour "+Colour[5]);
		c=cdb.getC(Colour[5]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		colour6colorLabel = mRgba.submat(4, 28, 144, 172);
		colour6colorLabel.setTo(converScalarHsv2Rgba(HSV));
		colour6colorLabel = mRgba.submat(28, 40,144,172);
		colour6colorLabel.setTo(colour6ContourColour);
		c.close();
	
		Log.d(TAG,"Getting colour "+Colour[6]);
		c=cdb.getC(Colour[6]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		colour7colorLabel = mRgba.submat(4, 28, 172, 200);
		colour7colorLabel.setTo(converScalarHsv2Rgba(HSV));
		colour7colorLabel = mRgba.submat(28, 40,172, 200);
		colour7colorLabel.setTo(colour7ContourColour);
		c.close();
	
		Log.d(TAG,"Getting colour "+Colour[7]);
		c=cdb.getC(Colour[7]);
		HSV=new Scalar(c.getInt(Hidx),c.getInt(Sidx),c.getInt(Vidx),255);
		colour8colorLabel = mRgba.submat(4, 28, 200, 228);
		colour8colorLabel.setTo(converScalarHsv2Rgba(HSV));
		colour8colorLabel = mRgba.submat(28, 40,200, 228);
		colour8colorLabel.setTo(colour8ContourColour);
		c.close();
	
	}
	
	public void updatevalues(Mat mRgba)
	{
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Iterator<MatOfPoint> each = mcolour1contours.iterator();
        double maxArea = 0;
        Imgproc.pyrDown(mRgba, mPyrDownMat);
        Imgproc.pyrDown(mPyrDownMat, mPyrDownMat);
        /*
        if (cnt==0){
        	Imgproc.cvtColor(mRgba, imgnew, Imgproc.COLOR_RGB2GRAY);
        	Imgproc.cvtColor(mRgba, imgres, Imgproc.COLOR_RGB2GRAY);
        	//imgnew=mRgba;
        	//imgres=mRgba;
        	cnt=1;
        	Log.d("result111", "Entered case 0");
        }
        else if (cnt>0 && cnt<10){
        	cnt+=1;
        }
        else{
        	imgnew.copyTo(imgold);
        	Imgproc.cvtColor(mRgba, imgnew, Imgproc.COLOR_RGB2GRAY);
        	//imgnew=mRgba;
        	Core.absdiff(imgnew, imgold, imgres);
            res=Core.sumElems(imgres);
            Log.d("result111", " "+ res);
            cnt=1;
        }*/
        
        List<Mat> hsv_planes = new ArrayList<Mat>(3);	
        //Mat imv=new Mat();
        Imgproc.cvtColor(mPyrDownMat, mHsvMat, Imgproc.COLOR_RGB2HSV_FULL);
        Core.split(mHsvMat, hsv_planes);
        //Mat mp=Converters.vector_Mat_to_Mat(hsv_planes);
        //hsv_planes[2];
        Imgproc.equalizeHist((Mat) hsv_planes.toArray()[2], (Mat) hsv_planes.toArray()[2]);
        Core.merge(hsv_planes, mHsvMat);
        mHSVMatMain=mHsvMat;
        
        // Processing colour 1
        mcolour1contours.clear();
        Core.inRange(mHsvMat, mcolour1LowerBound, mcolour1UpperBound, mcolour1Mask);
        Imgproc.dilate(mcolour1Mask, mcolour1DilatedMask, new Mat());
        Imgproc.findContours(mcolour1DilatedMask, contours, mcolour1Hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        // Find max contour area
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
                maxArea = area;
        }
        // Filter contours by area and resize to fit the original image size
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) > mMinContourArea*maxArea) {
                Core.multiply(contour, new Scalar(4,4), contour);
                mcolour1contours.add(contour);
            }
        }
        Log.d(TAG, "colour1 Contours count: " + mcolour1contours.size());
        Log.d(TAG,"Done with "+Colour[0]);

        // Processing colour 2
        contours.clear();
        mcolour2contours.clear();
        mHsvMat=mHSVMatMain;
        Core.inRange(mHsvMat, mcolour2LowerBound, mcolour2UpperBound, mcolour2Mask);
        Imgproc.dilate(mcolour2Mask, mcolour2DilatedMask, new Mat());
        Imgproc.findContours(mcolour2DilatedMask, contours, mcolour2Hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        // Find max contour area
        maxArea = 0;
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
                maxArea = area;
        }
        // Filter contours by area and resize to fit the original image size
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) > mMinContourArea*maxArea) {
                Core.multiply(contour, new Scalar(4,4), contour);
                mcolour2contours.add(contour);
            }
        }
        Log.d(TAG, "colour2 Contours count: " + mcolour2contours.size());
        Log.d(TAG,"Done with "+Colour[1]);

        // Processing colour 3
        contours.clear();
        mcolour3contours.clear();
        mHsvMat=mHSVMatMain;
        Core.inRange(mHsvMat, mcolour3LowerBound, mcolour3UpperBound, mcolour3Mask);
        Imgproc.dilate(mcolour3Mask, mcolour3DilatedMask, new Mat());
        Imgproc.findContours(mcolour3DilatedMask, contours, mcolour3Hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        // Find max contour area
        maxArea = 0;
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
                maxArea = area;
        }
        // Filter contours by area and resize to fit the original image size
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) > mMinContourArea*maxArea) {
                Core.multiply(contour, new Scalar(4,4), contour);
                mcolour3contours.add(contour);
            }
        }
        Log.d(TAG, "colour3 Contours count: " + mcolour3contours.size());
        Log.d(TAG,"Done with "+Colour[2]);

        // Processing colour 4
        contours.clear();
        mcolour4contours.clear();
        mHsvMat=mHSVMatMain;
        Core.inRange(mHsvMat, mcolour4LowerBound, mcolour4UpperBound, mcolour4Mask);
        Imgproc.dilate(mcolour4Mask, mcolour4DilatedMask, new Mat());
        Imgproc.findContours(mcolour4DilatedMask, contours, mcolour4Hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        // Find max contour area
        maxArea = 0;
        int id=-1, maxID=0;
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            id=id+1;
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
            {    
            	maxArea = area;
            	maxID=id;
            }
        }
        // Filter contours by area and resize to fit the original image size
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) > mMinContourArea*maxArea) {
                Core.multiply(contour, new Scalar(4,4), contour);
                mcolour4contours.add(contour);
            }
        }
        Log.d(TAG, "colour4 Contours count: " + mcolour4contours.size());
        Log.d(TAG,"Done with "+Colour[3]);
        if (contours.size()>0)
        {
        	contours.get(maxID).convertTo(maxMOP2f,CvType.CV_32FC2);
        	RotatedRect maxRect=Imgproc.minAreaRect(maxMOP2f);    	
            centroidx= maxRect.center.x;	//Centroidx
            centroidy= maxRect.center.y;	//Centroidy
            double cwidth=maxRect.size.width;	//Width of rect
            double cheight=maxRect.size.height;	//Height of rect
            sqside = 0;
            //Now define side of ROI square	
            if (cwidth>cheight)
            	sqside=cwidth;
            else
            	sqside=cheight;
            centroidy=centroidy+2*sqside;
            //Define opposite points of ROI square;
            Point p1 = new Point(centroidx-2*sqside,centroidy-2*sqside);
            Point p2 = new Point(centroidx+2*sqside,centroidy+2*sqside); 
            //Draw ROI square
        	Core.ellipse(mRgba, maxRect, colour4ContourColour,2);
            Core.rectangle(mRgba, p1, p2, ROIContourColour);
            colour4RR.add(maxRect);
        	Log.d(TAG+":size1",colour4RR.size()+"");
        }
        
        
        // Processing colour 5
        contours.clear();
        mcolour5contours.clear();
        mHsvMat=mHSVMatMain;
        Core.inRange(mHsvMat, mcolour5LowerBound, mcolour5UpperBound, mcolour5Mask);
        Imgproc.dilate(mcolour5Mask, mcolour5DilatedMask, new Mat());
        Imgproc.findContours(mcolour5DilatedMask, contours, mcolour5Hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        // Find max contour area
        maxArea = 0;
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
                maxArea = area;
        }
        // Filter contours by area and resize to fit the original image size
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) > mMinContourArea*maxArea) {
                Core.multiply(contour, new Scalar(4,4), contour);
                mcolour5contours.add(contour);
            }
        }
        Log.d(TAG, "colour5 Contours count: " + mcolour5contours.size());
        Log.d(TAG,"Done with "+Colour[4]);

        // Processing colour 6
        contours.clear();
        mcolour6contours.clear();
        mHsvMat=mHSVMatMain;
        Core.inRange(mHsvMat, mcolour6LowerBound, mcolour6UpperBound, mcolour6Mask);
        Imgproc.dilate(mcolour6Mask, mcolour6DilatedMask, new Mat());
        Imgproc.findContours(mcolour6DilatedMask, contours, mcolour6Hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        // Find max contour area
        maxArea = 0;
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
                maxArea = area;
        }
        // Filter contours by area and resize to fit the original image size
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) > mMinContourArea*maxArea) {
                Core.multiply(contour, new Scalar(4,4), contour);
                mcolour6contours.add(contour);
            }
        }
        Log.d(TAG, "colour 6 Contours count: " + mcolour6contours.size());
        Log.d(TAG,"Done with "+Colour[5]);

        // Processing colour 7
        contours.clear();
        mcolour7contours.clear();
        mHsvMat=mHSVMatMain;
        Core.inRange(mHsvMat, mcolour7LowerBound, mcolour7UpperBound, mcolour7Mask);
        Imgproc.dilate(mcolour7Mask, mcolour7DilatedMask, new Mat());
        Imgproc.findContours(mcolour7DilatedMask, contours, mcolour7Hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        // Find max contour area
        maxArea = 0;
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
                maxArea = area;
        }
        // Filter contours by area and resize to fit the original image size
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) > mMinContourArea*maxArea) {
                Core.multiply(contour, new Scalar(4,4), contour);
                mcolour7contours.add(contour);
            }
        }
        Log.d(TAG, "colour7 Contours count: " + mcolour7contours.size());
        Log.d(TAG,"Done with "+Colour[6]);

        // Processing colour 8
        contours.clear();
        mcolour8contours.clear();
        mHsvMat=mHSVMatMain;
        Core.inRange(mHsvMat, mcolour8LowerBound, mcolour8UpperBound, mcolour8Mask);
        Imgproc.dilate(mcolour8Mask, mcolour8DilatedMask, new Mat());
        Imgproc.findContours(mcolour8DilatedMask, contours, mcolour8Hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
        // Find max contour area
        maxArea = 0;
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
                maxArea = area;
        }
        // Filter contours by area and resize to fit the original image size
        each = contours.iterator();
        while (each.hasNext()) {
            MatOfPoint contour = each.next();
            if (Imgproc.contourArea(contour) > mMinContourArea*maxArea) {
                Core.multiply(contour, new Scalar(4,4), contour);
                mcolour8contours.add(contour);
            }
        }
        Log.d(TAG, "colour8 Contours count: " + mcolour5contours.size());
        Log.d(TAG,"Done with "+Colour[7]);

        // TODO:Processing colour[8] or colour 9 to be written. For now, pack.
        
        // Post processing
        showColourBoxes(mRgba);
        //Imgproc.drawContours(mRgba, mcolour1contours, -1, colour1ContourColour);
        //Imgproc.drawContours(mRgba, mcolour2contours, -1, colour2ContourColour);
        
        colour1RR.clear();
        colour1count=0;
        for(int i=0;i<mcolour1contours.size();i++)
        {
        	mcolour1contours.get(i).convertTo(m1c, CvType.CV_32FC2);
        	RotatedRect rect=Imgproc.minAreaRect(m1c);
        	//First check if within region of interest.. Define Region of interest as above thumb?
        	if (rect.center.x<centroidx+2*sqside&&rect.center.x>centroidx-2*sqside&&rect.center.y>centroidy-2*sqside&&rect.center.y<centroidy+2*sqside)
        	{	
        		colour1RR.add(rect);
        		colour1count=colour1count+1;//Count the number of colour1Conturs
            	Core.ellipse(mRgba, rect, colour1ContourColour,2);//Draw Ellipse

        	}
        }
        //Tag:colour1Count to see current colour1 contours within ROI.
        Log.d(TAG+":colour1count",colour1count+"");
        
        colour2RR.clear();
        colour2count=0;
        for(int i=0;i<mcolour2contours.size();i++)
        {
        	mcolour2contours.get(i).convertTo(m2c, CvType.CV_32FC2);
        	RotatedRect rect=Imgproc.minAreaRect(m2c);
        	if (rect.center.x<centroidx+2*sqside&&rect.center.x>centroidx-2*sqside&&rect.center.y>centroidy-2*sqside&&rect.center.y<centroidy+2*sqside)
        	{
        		colour2RR.add(rect);
        		colour2count=colour2count+1;//Count the number of colour1Conturs
            	Core.ellipse(mRgba, rect, colour2ContourColour,2);
        	}
        }
        Log.d(TAG+":colour2count",colour2count+"");

        colour3RR.clear();
        colour3count=0;
        for(int i=0;i<mcolour3contours.size();i++)
        {	
        	mcolour3contours.get(i).convertTo(m3c, CvType.CV_32FC2);
        	RotatedRect rect=Imgproc.minAreaRect(m3c);
        	if (rect.center.x<centroidx+2*sqside&&rect.center.x>centroidx-2*sqside&&rect.center.y>centroidy-2*sqside&&rect.center.y<centroidy+2*sqside)
        	{
        		colour3RR.add(rect);
        		colour3count=colour3count+1;//Count the number of colour1Conturs
            	Core.ellipse(mRgba, rect, colour3ContourColour,2);
        	}
        }
        Log.d(TAG+":colour3count",colour3count+"");

        //colour4RR.clear();
        colour4count=0;
        for(int i=0;i<mcolour4contours.size();i++)
        {	//Log.d(TAG+":DGcon",mcolour4contours.size()+"");
        	mcolour4contours.get(i).convertTo(m4c, CvType.CV_32FC2);
        	RotatedRect rect=Imgproc.minAreaRect(m4c);
        	if (rect.center.x<centroidx+2*sqside&&rect.center.x>centroidx-2*sqside&&rect.center.y>centroidy-2*sqside&&rect.center.y<centroidy+2*sqside)
        	{	
        		colour4RR.add(rect);
        		colour4count=colour4count+1;
            	Core.ellipse(mRgba, rect, colour4ContourColour,2);

        	}
        }
        Log.d(TAG+":colour4count",colour4count+"");

        colour5RR.clear();
        colour5count=0;
        for(int i=0;i<mcolour5contours.size();i++)
        {
        	mcolour5contours.get(i).convertTo(m5c, CvType.CV_32FC2);
        	RotatedRect rect=Imgproc.minAreaRect(m5c);
        	if (rect.center.x<centroidx+2*sqside&&rect.center.x>centroidx-2*sqside&&rect.center.y>centroidy-2*sqside&&rect.center.y<centroidy+2*sqside)
        	{
        		colour5RR.add(rect);
        		colour5count=colour5count+1;
            	Core.ellipse(mRgba, rect, colour5ContourColour,2);

        	}
        }
        Log.d(TAG+":colour5count",colour5count+"");
        
        colour6RR.clear();
        colour6count=0;
        for(int i=0;i<mcolour6contours.size();i++)
        {	
    		mcolour6contours.get(i).convertTo(m6c, CvType.CV_32FC2);
        	RotatedRect rect=Imgproc.minAreaRect(m6c);
        	if (rect.center.x<centroidx+2*sqside&&rect.center.x>centroidx-2*sqside&&rect.center.y>centroidy-2*sqside&&rect.center.y<centroidy+2*sqside)
        	{
        		colour6RR.add(rect);
        		colour6count=colour6count+1;
            	Core.ellipse(mRgba, rect, colour6ContourColour,2);
        	}
        }
        Log.d(TAG+":colour6count",colour6count+"");

        colour7RR.clear();
        colour7count=0;
        for(int i=0;i<mcolour7contours.size();i++)
        {	
        	mcolour7contours.get(i).convertTo(m7c, CvType.CV_32FC2);
        	RotatedRect rect=Imgproc.minAreaRect(m7c);
        	if (rect.center.x<centroidx+2*sqside&&rect.center.x>centroidx-2*sqside&&rect.center.y>centroidy-2*sqside&&rect.center.y<centroidy+2*sqside)
        	{
        		colour7RR.add(rect);
        		colour7count=colour7count+1;
            	//Core.ellipse(mRgba, rect, colour7ContourColour,2);
        	}
        }
        Log.d(TAG+":colour7count",colour7count+"");

        colour8count=0;
        colour8RR.clear();
        for(int i=0;i<mcolour8contours.size();i++)
        {	
        	mcolour8contours.get(i).convertTo(m8c, CvType.CV_32FC2);
        	RotatedRect rect=Imgproc.minAreaRect(m8c);
        	if (rect.center.x<centroidx+2*sqside&&rect.center.x>centroidx-2*sqside&&rect.center.y>centroidy-2*sqside&&rect.center.y<centroidy+2*sqside)
        	{
        		colour8RR.add(rect);  		
        		colour8count=colour8count+1;
            	//Core.ellipse(mRgba, rect, colour8ContourColour,2);
        	}
        }
        Log.d(TAG+":colour8count",colour8count+"");

        Log.d(TAG+":colourCount",colour1count+" "+colour2count+" "+colour3count+" "+colour4count+" "+colour5count+" "+colour6count+" "+colour7count+" "+colour8count+" ");
        
        if (colour1count==0&&colour2count==0&&colour4count==1)
        	Spout="zero";
        else if (colour1count==1&&colour2count==0)
        	Spout="one";
        else if (colour1count==1&&colour2count==1)
        	Spout="two";
        else if (colour1count==2&&colour2count==1)
        	Spout="three";
        else if (colour1count==2&&colour2count==2&&colour5count==0)
        	Spout="four";
        else if (colour1count>=2&&colour2count==2&&colour5count==1)
        	Spout="five";
        else
        	Spout=" ";
        
        Log.d(TAG+":SpeakOut",Spout);
        
        clench=cDetector.isClenched();
        oc=cDetector.openCount();
        isStraight=cDetector.isHandStraight();
        Log.d(TAG+":config:clench",clench+"");	
        Log.d(TAG+":config:oc",oc+"");
        Log.d(TAG+":config:straight",isStraight+"");
    	Log.d(TAG+":size",colour4RR.size()+"");

        if (colour4RR.size()>0)
        	Log.d(TAG+":angle",colour4RR.size()+"");
	}
	
    
	private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);

        return new Scalar(pointMatRgba.get(0, 0));
    }
	
//	private Scalar converScalarRgba2Hsv(Scalar rgbaColor) {
//	    Mat pointMatHsv = new Mat();
//	    Mat pointMatRgba = new Mat(1, 1, CvType.CV_8UC3, rgbaColor);
//	    Imgproc.cvtColor(pointMatRgba, pointMatHsv, Imgproc.COLOR_RGB2HSV_FULL, 4);
//	
//	    return new Scalar(pointMatHsv.get(0, 0));
//	}
	
}	
