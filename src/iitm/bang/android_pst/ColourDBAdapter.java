package iitm.bang.android_pst;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ColourDBAdapter {
	
	
	// Calls public void onCreate(SQLiteDatabase db)
	ColourDBHelper db_helper;
	// Database naming
	String DB_NAME="Colours";
	String TAB_NAME="colour_table";
	String TAG="Android_PST:ColourDBAdapter";
	String Colour[] ={"Pink","LBlue","Yellow","Green","Orange","Black","Red","Blue","DGreen"};
    // Do most of the communication
	// db_helper is just to do the upper-level 
	SQLiteDatabase db=null;
	
	public Integer N=1;			//The total count
	public Integer NO_OF_COLOURS=9;
	
	public ColourDBAdapter(Context context) 
	{
		 db_helper = new ColourDBHelper(context, DB_NAME, null, 1);
		 Cursor c1=getAllCs();
		 N=c1.getCount();			// N now has the count
		 Log.d(TAG,"Total no. of elements =>"+N);
		 if(N!=(NO_OF_COLOURS))
		 {
			 initDB();
		 }
		 Log.d(TAG,"Total no. of elements =>"+N);
		 c1.close();
		 close();					// Closing the link for Qs();
	}
	
	public void open() throws SQLException 
    {
       //open database in reading/writing mode
       db = db_helper.getWritableDatabase();
    } 

	public void close() 
	{
	   if (db != null)
		   db.close();
	}	
	
	public void insertC(String Colour,Integer Hue,Integer Sat,Integer Val,Integer HueR,Integer SatR,Integer ValR)
	{
		// ContentValues which is like bundle
		ContentValues bag = new ContentValues();
		// Order matters. It should be as same as the columns
		// Contents of the bag will increase with every put statement
		bag.put("colour", Colour);
		bag.put("hue", Hue);
		bag.put("sat", Sat);
		bag.put("val", Val);
		bag.put("huer", HueR);
		bag.put("satr",SatR);
		bag.put("valr",ValR);
		bag.put("counter",1);
		
		open();
		//Insert into the table qbank the contents of the bag.
		long row=db.insert(TAB_NAME, null, bag);
		if(row!=-1)
			Log.d(TAG,"Inserted an entry => "+row);
		else
			Log.d(TAG,"Error while inserting");
		close();
		N=N+1;								// Update N
	}
	
	public void updateC(String Colour,Integer Hue,Integer Sat,Integer Val,Integer HueR,Integer SatR,Integer ValR)
	{
		Cursor c1=getC(Colour);
		Integer counter=c1.getInt(7);
		// ContentValues which is like bundle
		ContentValues bag = new ContentValues();
		// Order matters. It should be as same as the columns
		// Contents of the bag will increase with every put statement
		bag.put("hue", Hue);
		bag.put("sat", Sat);
		bag.put("val", Val);
		bag.put("huer", HueR);
		bag.put("satr",SatR);
		bag.put("valr",ValR);
		Log.d(TAG,"Currently the counter reads "+counter);
		bag.put("counter",counter+1);
		Log.d(TAG,"Incremented the counter to "+counter);
		
		open();
		//Insert into the table qbank the contents of the bag.
		long row=db.update(TAB_NAME,bag,"colour=?",new String []{Colour});
		if(row!=-1)
			Log.d(TAG,"Updated entry for "+Colour);
		else
			Log.d(TAG,"Failed operation while updating entry for "+Colour);
		close();
	}
	
	public Cursor getAllCs()
	{
		open();
		Log.d(TAG,"Asked to fetch the colour table");
		String query="SELECT * FROM ";
		query=query.concat(TAB_NAME);
		Cursor c1 = db.rawQuery(query, null);
		Log.d(TAG,"Fetched the colour table");
		return c1;
	}
	
	public void initDB()
	{
               //colour|hue|sat|val|huer|satr|valr|counter);";
		/*
		//Girish's Phone
	    insertC("LBlue",235,51,159,10,40,50);//Pink
	    insertC("DBlue",130,172,134,15,40,60);//LBlue
	    insertC("Green",46,201,167,15,30,50);//Yellow
	    insertC("DGreen",80,222,170,20,70,80);//Green
	    insertC("Yellow",15,119,180,15,40,56);//Orange
	    insertC("Black",70,207,197,15,38,255);
	    insertC("Red",70,207,197,15,38,255);
	    insertC("Pink",70,207,197,15,38,255);
	    insertC("Orange",70,207,197,15,38,255);
	    Log.d(TAG,"Initialized the DB");*/
		
		//Abraham's Phone
		insertC(Colour[0],232,190,153,23,85,255);//Pink
	    insertC(Colour[1],164,210,110,18,52,255);//LBlue
	    insertC(Colour[2],36,174,155,18,90,255);//Yellow
	    insertC(Colour[3],96,183,185,20,51,255);//Green
	    insertC(Colour[4],9,181,169,19,66,255);//Orange
	    insertC(Colour[5],137,100,68,15,65,60);//Black
	    insertC(Colour[6],70,207,197,15,38,255);
	    insertC(Colour[7],150,116,127,18,67,255);//Blue
	    insertC(Colour[8],70,207,197,15,38,255);
	    Log.d(TAG,"Initialized the DB");
		
	}
	
	public Cursor getC(String Colour)
	{	
		open();
		Log.d(TAG,"Got to fetch "+Colour);
		String query="SELECT * FROM "+TAB_NAME+" WHERE colour=?";
		Cursor c=null;
		c=db.rawQuery(query, new String[]{Colour});
		c.moveToNext();				// Moving the cursor forward
		Log.d(TAG,"Got it");
		close();
		return c;						// Returns null if failed
	}
	
	public void seeDB(Context context)
	{
		//TODO: Export Values to a text file
	}
}
