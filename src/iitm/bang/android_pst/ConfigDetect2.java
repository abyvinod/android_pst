
package iitm.bang.android_pst;

import java.util.List;
import java.lang.Math;


import org.opencv.core.RotatedRect;

import android.util.Log;

public class ConfigDetect2 {
//LittleMiddlebackcolour=c1
//LittleMiddlefrontcolour=c2
//RingIndexbackcolour=c3
//RingIndexfrontcolour=c4
//Thumbfrontcolour=c5
//Thumbbackcolour=c6
//Flankcolour=c7
//Wristcolour=c8
	//TODO: Do ellipse merging and hard relative decisions to ensure removal of false positives. Eg. Index finger front cannot be confused with thumb
	public double palmTresholdArea=15;
	public double thresholdAngle=30;
	public double thumbThresholdArea;
	public double seperationxTreshold;
	public double seperationyTreshold;	
	public double LPx=0;
	public double LPy=0;
	public double LParea=0;
	//>means to the right, to the bottom.
	List<RotatedRect> c1RectList;
	List<RotatedRect> c2RectList;
	List<RotatedRect> c3RectList;
	List<RotatedRect> c4RectList;
	List<RotatedRect> c5RectList;
	List<RotatedRect> c6RectList;
	List<RotatedRect> c7RectList;
	List<RotatedRect> c8RectList;
	
	public ConfigDetect2(List<RotatedRect>c1RR,List<RotatedRect>c2RR,List<RotatedRect>c3RR,List<RotatedRect>c4RR,List<RotatedRect>c5RR,List<RotatedRect>c6RR,List<RotatedRect>c7RR,List<RotatedRect>c8RR)
	{
		c1RectList=c1RR;
		c2RectList=c2RR;
		c3RectList=c3RR;
		c4RectList=c4RR;
		c5RectList=c5RR;
		c6RectList=c6RR;
		c7RectList=c7RR;
		c8RectList=c8RR;

	}
	public void mergeEllipses(List<RotatedRect> r1)
	{
	}
	
	public void ellipseWithinOther()
	{
		//c1RectList.get(0).angle
	}
	
	public boolean isClenched()
	{	
		if (c1RectList.size()==2&&c3RectList.size()==2)
			return true;
		else
			return false;
	}
	
	public boolean isThumbOpen()
	{
		if (c5RectList.size()==1&&c5RectList.get(0).size.area()>thumbThresholdArea)
			return true;
		else 
			return false;			
	}
	
	public boolean isHandStraight()
	{	if (c8RectList.size()>0)
		{
			if(/*c8RectList.get(0).angle<90-thresholdAngle&&*/c8RectList.get(0).angle<90)
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	public int openCount()
	{
		return c2RectList.size()+c4RectList.size();
	}
	
	public boolean isIndMidSeperate()
	{
		if (java.lang.Math.abs(c2RectList.get(0).center.x-c2RectList.get(0).center.x)>seperationxTreshold||java.lang.Math.abs(c2RectList.get(0).center.y-c2RectList.get(0).center.y)>seperationyTreshold)
		{
			return true;
		}
		else
			return false;
		
	}
	
	public char guessGesture()
	{
		int oc=openCount();
		if (isClenched())//If fist is clenched
			{
			//AEMNST
			//TODO:Check if comparing relative area works for E vs S.
				if (c5RectList.size()==0)//If front of thumb invisible
					return 'S';
				else if (c5RectList.get(0).size.area()>thumbThresholdArea)
				{	//TODO:Test and Review A vs E.
					if (c5RectList.get(0).center.x>c4RectList.get(0).center.x&&c5RectList.get(0).center.x>c4RectList.get(1).center.x)
					{	//If front of thumb is somewhat visible and to the right of index and ring
						return 'A';
					}
					else
						return 'E';
				}
				else if (c5RectList.get(0).size.area()<thumbThresholdArea)//If thumb ellipse is very small
				{
					if (c5RectList.get(0).center.x<c4RectList.get(0).center.x&&c5RectList.get(0).center.x<c4RectList.get(1).center.x)
							{	
								return 'M';//If thumb is to the left of both index and ring
							}
					else if (c5RectList.get(0).center.x>c2RectList.get(0).center.x&&c5RectList.get(0).center.x>c2RectList.get(1).center.x&&(c5RectList.get(0).center.x<c4RectList.get(0).center.x||c5RectList.get(1).center.x<c4RectList.get(1).center.x))
							{
								return 'T';//If thumb is to the right of both little and middle and at least one of the other two ellipses is to the right of thumb (prevent confusion with A)
							}
					else return 'N';
				}
			}
			else if (oc==4)
			{
				//B
				return 'B';
			}
			else if (oc==3)
			{	//FW
				if (c2RectList.size()==2)
					return 'F';
				else if (c4RectList.size()==2)
					return 'W';
							
			}
			else if (oc==2)
			{
				//HKPRUV
				if (isIndMidSeperate())//Is there spacing between index and middle finger?
				{
					//PKV
					if (isThumbOpen())
						return 'V';
					else if (c6RectList.get(0).center.y<c2RectList.get(0).center.y)
					{
						//Thumb is below middle
						return 'K';
					}
					else 
					{
						//Thumb is above middle 
						return 'P';
					}
					
				}
				else
				{
					//URH
					if (c2RectList.get(0).center.x>c4RectList.get(0).center.x)
					{
						//Centre of middle to the right of centre of index.
						return 'R';
					}
					else
					{
						if (isHandStraight())//Is Hand straight up?
						{
							return 'U';
						}
						else //Is hand aligned??
						{
							return 'H';
						}
					}
				}
			}	
			else if (oc==1)	
			{
				//LDQXYI
				if (c2RectList.size()==1)
					if(isThumbOpen())
						return 'Y';
					else 
						if (isHandStraight())
						{
							return 'I';
						}
						else 
						{
							return 'J';
						}
				if (c4RectList.size()==1)
				{
					if(isThumbOpen())
					{
						//DL
						return 'D';
					}
					else 
					{
						//QX
						return'Q';
					}
				}
			}
			else//Default case 
				return '1';
		return '0';
		}
						
	//TODO:CDGLOQXZ
	public void locateLowerPalm()
	{
		double xtemp=0, ytemp=0, xcenter=0, ycenter=0;
		int i;
		double areatemp=0;
		for (i=0;i< c6RectList.size();i++)
		{
			ytemp = c6RectList.get(i).center.y;
			xtemp = c6RectList.get(i).center.x;
			areatemp=c6RectList.get(i).size.area();
			if (LParea<areatemp)
				LParea=areatemp;
			xcenter=xcenter+xtemp;
			ycenter=ycenter+ytemp;
		}	
		LPx=xcenter/(i+1);
		LPy=ycenter/(i+1);
	}
	
	public boolean isPalmopen()
	{
		int i=0;
		double areatemp;
		double UtoLRatio=0.2;
		double UParea=0;
		for (i=0;i< c6RectList.size();i++)
		{
			areatemp=c6RectList.get(i).size.area();
			if (UParea<areatemp)
				UParea=areatemp;
		}
		if (UParea>UtoLRatio*LParea)
			return true;
		else
			return false;
	}
/*	
	public boolean isPalmClenched()
	{
		int i=0;
		double areatemp=0;
		
		double RIbackArea1=0;
		double RIbackArea2=0;
		double LMbackArea1=0;
		double LMbackArea2=0;
		int ind1=0, ind2=0, ind3=0, ind4=0;
		
		if (c3RectList.size()<2||c1RectList.size()<2)
			return false;
		for (i=0;i<c3RectList.size();i++)
		{
			areatemp=c3RectList.get(i).size.area();
			if (RIbackArea1<areatemp)
			{
				RIbackArea1=areatemp;
				ind1=i;
			}
			else if (RIbackArea2<areatemp)
			{
				RIbackArea2=areatemp;
				ind2=i;
			}
		}
		for (i=0;i<c1RectList.size();i++)
		{
			areatemp=c1RectList.get(i).size.area();
			if (LMbackArea1<areatemp)
			{
				LMbackArea1=areatemp;
				ind3=i;
			}
			else if (LMbackArea2<areatemp)
			{
				LMbackArea2=areatemp;
				ind4=i;
			}
			
		}
		*/
		//TODO:GIRISH VERIFY THIS
//		return true;
//	}
}
